﻿using pokerCSharp.entities.enums;
using pokerCSharp.views.administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.viewmodel.administration
{
    public class LayoutVM
    {
        public LayoutVM(LayoutV layout, String type)
        {
            if (type == TypeViewEnum.CARD.ToString())
            {
                createCardAdministationVM(layout);
            }
            else if (type == TypeViewEnum.SYMBOL.ToString())
            {
                createSymbolAdministationVM(layout);
            }
            else if (type == TypeViewEnum.RANKING.ToString())
            {
                createRankingAdministationVM(layout);
            }
            else if (type == TypeViewEnum.TABLE.ToString())
            {
                createTableAdministationVM(layout);
            }
            else if (type == TypeViewEnum.USER.ToString())
            {
                createUserAdministationVM(layout);
            }
        }

        public LayoutVM(LayoutV layout, String type, bool extend)
        {
            if (type == TypeViewEnum.SYMBOL.ToString())
            {
                SymbolAdministrationVM symbolAdministrationVM = new SymbolAdministrationVM(layout, true);
                layout.DataContext = symbolAdministrationVM;
            }

        }

        private void createCardAdministationVM(LayoutV layout)
        {
            CardAdministrationVM cardAdministrationVM = new CardAdministrationVM(layout);

            layout.DataContext = cardAdministrationVM;
        }

        private void createRankingAdministationVM(LayoutV layout)
        {
            RankingAdministrationVM rankingAdministrationVM = new RankingAdministrationVM(layout);

            layout.DataContext = rankingAdministrationVM;
        }

        private void createSymbolAdministationVM(LayoutV layout)
        {
            SymbolAdministrationVM symbolAdministrationVM = new SymbolAdministrationVM(layout);

            layout.DataContext = symbolAdministrationVM;
        }
        private void createUserAdministationVM(LayoutV layout)
        {
            UserAdministrationVM userAdministrationVM = new UserAdministrationVM(layout);

            layout.DataContext = userAdministrationVM;
        }
        private void createTableAdministationVM(LayoutV layout)
        {
            TableAdministrationVM tableAdministrationVM = new TableAdministrationVM(layout);

            layout.DataContext = tableAdministrationVM;
        }
    }
}
