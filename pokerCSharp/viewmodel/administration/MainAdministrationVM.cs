﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using pokerCSharp.views;
using pokerCSharp.views.administration;
using pokerCSharp.entities;
using pokerCSharp.entities.enums;
using pokerCSharp.database;

namespace pokerCSharp.viewmodel
{
    class MainAdministrationVM
    {
        private MainAdministrationV MainAdministrationV { get; set; }

        public MainAdministrationVM() { }

        public MainAdministrationVM(MainAdministrationV view)
        {
            this.MainAdministrationV = view;
            initBtnListener();
        }

        public void initBtnListener()
        {
            //this.mainMenuView.UCMainMenuView.BtnAdministration.Click += clickAdministration;
            this.MainAdministrationV.btnCard.Click += clickCard;
            this.MainAdministrationV.btnUser.Click += clickUser;
            this.MainAdministrationV.btnTable.Click += clickTable;
            this.MainAdministrationV.btnPatternCard.Click += clickPatternCard;
            this.MainAdministrationV.btnSymbol.Click += clickSymbol;
            this.MainAdministrationV.btnRanking.Click += clickRanking;
        }

        private void clickUser(object sender, RoutedEventArgs e)
        {
            this.MainAdministrationV.NavigationService.Navigate(new LayoutV(TypeViewEnum.USER.ToString()));
        }

        private void clickCard(object sender, RoutedEventArgs e)
        {
            this.MainAdministrationV.NavigationService.Navigate(new LayoutV(TypeViewEnum.CARD.ToString()));
        }

        private void clickTable(object sender, RoutedEventArgs e)
        {
            this.MainAdministrationV.NavigationService.Navigate(new LayoutV(TypeViewEnum.TABLE.ToString()));
        }
        private void clickPatternCard(object sender, RoutedEventArgs e)
        {
            this.MainAdministrationV.NavigationService.Navigate(new LayoutV(TypeViewEnum.PATTERNCARD.ToString()));
        }

        private void clickRanking(object sender, RoutedEventArgs e)
        {
            this.MainAdministrationV.NavigationService.Navigate(new LayoutV(TypeViewEnum.RANKING.ToString()));
        }

        private void clickSymbol(object sender, RoutedEventArgs e)
        {
            this.MainAdministrationV.NavigationService.Navigate(new LayoutV(TypeViewEnum.SYMBOL.ToString()));
        }
    }
}
