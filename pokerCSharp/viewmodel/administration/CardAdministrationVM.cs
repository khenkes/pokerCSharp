﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pokerCSharp.views.administration;
using pokerCSharp.entities;
using System.Windows.Controls;
using pokerCSharp.database;
using System.Windows;
using System.Text.RegularExpressions;
using pokerCSharp.views.usercontrols;
using pokerCSharp.entities.enums;

namespace pokerCSharp.viewmodel.administration
{
    public class CardAdministrationVM
    {
        public LayoutV LayoutV { get; set; }
        public CardUC UC { get; set; }
        public CardLUC LUC { get; set; }

        public CardAdministrationVM() { }

        public CardAdministrationVM(LayoutV view)
        {
            this.LayoutV = view;
            InitUC();
            InitLUC();
            InitActions();
            InitSpecificActions();
        }

        //private MySQLManager<Schedule> scheduleManager = new MySQLManager<Schedule>();


        private void ItemsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Card item = (e.AddedItems[0] as Card);

                if (item.Ranking == null)
                {
                    item.Ranking = new Ranking();
                }
                if (item.Symbol == null)
                {
                    item.Symbol = new Symbol();
                }

                MainVM.Card = item;

                this.UC.Card = MainVM.Card;
            }
        }

        private void InitUC()
        {
            this.UC = new CardUC();

            UC.SetValue(Grid.RowProperty, 1);
            UC.SetValue(Grid.ColumnProperty, 0);
            UC.SetValue(Grid.RowSpanProperty, 2);
            UC.SetValue(Grid.ColumnSpanProperty, 3);
            UC.SetValue(FrameworkElement.NameProperty, "UC");

            this.LayoutV.mainGrid.Children.Add(UC);
        }

        private async void InitLUC()
        {
            this.LUC = new CardLUC();

            LUC.SetValue(Grid.RowProperty, 0);
            LUC.SetValue(Grid.ColumnProperty, 3);
            LUC.SetValue(Grid.RowSpanProperty, 7);
            LUC.SetValue(FrameworkElement.NameProperty, "LUC");

            this.LayoutV.mainGrid.Children.Add(LUC);

            this.LUC.LoadItem((await MainVM.CardManager.Get()).ToList());
        }

        private void InitActions()
        {
            this.LayoutV.btnAdd.Click += btnAdd_Click;
            this.LayoutV.btnUpdate.Click += btnUpdate_Click;
            this.LayoutV.btnDel.Click += btnDel_Click;
            this.LUC.ItemsList.SelectionChanged += ItemsList_SelectionChanged;
        }

        private void InitSpecificActions()
        {
            this.UC.symbolButton.Click += btnSymbolAdministration_Click;
        }

        private void btnSymbolAdministration_Click(object sender, RoutedEventArgs e)
        {
            MainVM.Symbol = this.UC.Card.Symbol;
            this.LayoutV.NavigationService.Navigate(new LayoutV(TypeViewEnum.SYMBOL.ToString(), true));
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (this.checkValidity(this.UC.Card))
            {
                await MainVM.CardManager.Insert(this.UC.Card);
                InitLUC();
            }
        }

        private async void btnDel_Click(object sender, RoutedEventArgs e)
        {
            await MainVM.CardManager.Delete(this.UC.Card);
            InitLUC();
            InitUC();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (this.checkValidity(this.UC.Card))
            {
                updateItem();
                InitLUC();
            }
        }

        public async void updateItem()
        {
            await MainVM.CardManager.Update(this.UC.Card);
        }

        private Boolean checkValidity(Card card)
        {
            var regexName = new Regex(@"[a-zA-Z ]{3,30}");
            //var regexSalary = new Regex(@"\d+(,\d{1,2})?");

            if (regexName.IsMatch(card.Sprite)/* && regexSalary.IsMatch(job.Salary.ToString())*/)
            {
                return true;
            }
            else
            {
                System.Windows.MessageBox.Show("Please check fields");
                return false;
            }
        }
    }
}

