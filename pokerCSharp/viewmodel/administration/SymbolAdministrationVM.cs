﻿using pokerCSharp.database;
using pokerCSharp.entities;
using pokerCSharp.views.administration;
using pokerCSharp.views.usercontrols;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace pokerCSharp.viewmodel.administration
{
    class SymbolAdministrationVM
    {
        public LayoutV LayoutV { get; set; }
        public SymbolUC UC { get; set; }
        public SymbolLUC LUC { get; set; }
        private bool Extend { get; set; }

        public SymbolAdministrationVM() { }

        public SymbolAdministrationVM(LayoutV view)
        {
            this.LayoutV = view;
            InitUC();
            InitLUC();
            InitActions();
        }

        public SymbolAdministrationVM(LayoutV view, Boolean extend)
        {
            Extend = extend;
            this.LayoutV = view;
            InitUC();
            InitLUC();
            InitActions();
        }

        //private MySQLManager<Schedule> scheduleManager = new MySQLManager<Schedule>();

        private void InitSpecificView()
        {
            this.LayoutV.mainGrid.Children.Remove(this.LayoutV.btnAdd);
            this.LayoutV.mainGrid.Children.Remove(this.LayoutV.btnDel);
            this.LayoutV.mainGrid.Children.Remove(this.LayoutV.btnUpdate);
            this.LayoutV.mainGrid.Children.Add(createSelectButton());
        }

        private void ItemsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                Symbol item = (e.AddedItems[0] as Symbol);

                MainVM.Symbol = item;

                this.UC.Symbol = MainVM.Symbol;
            }
        }

        private async void InitLUC()
        {
            this.LUC = new SymbolLUC();

            LUC.SetValue(Grid.RowProperty, 0);
            LUC.SetValue(Grid.ColumnProperty, 3);
            LUC.SetValue(Grid.RowSpanProperty, 7);
            LUC.SetValue(FrameworkElement.NameProperty, "LUC");

            this.LayoutV.mainGrid.Children.Add(LUC);

            this.LUC.LoadItem((await MainVM.SymbolManager.Get()).ToList());
        }

        private void InitUC()
        {
            this.UC = new SymbolUC();

            UC.SetValue(Grid.RowProperty, 1);
            UC.SetValue(Grid.ColumnProperty, 0);
            UC.SetValue(Grid.RowSpanProperty, 2);
            UC.SetValue(Grid.ColumnSpanProperty, 3);
            UC.SetValue(FrameworkElement.NameProperty, "UC");

            this.LayoutV.mainGrid.Children.Add(UC);

            if (Extend)
            {
                InitSpecificView();
            }
        }

        private Button createSelectButton()
        {
            Button btnSelectSymbol = new Button();

            btnSelectSymbol.SetValue(Grid.RowProperty, 5);
            btnSelectSymbol.SetValue(Grid.ColumnProperty, 1);
            btnSelectSymbol.SetValue(ContentControl.ContentProperty, "Select");
            btnSelectSymbol.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Center);
            btnSelectSymbol.SetValue(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Center);
            btnSelectSymbol.SetValue(FrameworkElement.WidthProperty, 75.0);
            btnSelectSymbol.SetValue(FrameworkElement.NameProperty, "btnSelectSymbol");

            btnSelectSymbol.Click += btnSelectSymbol_Click;

            return btnSelectSymbol;
        }

        private void InitActions()
        {
            this.LayoutV.btnAdd.Click += btnAdd_Click;
            this.LayoutV.btnUpdate.Click += btnUpdate_Click;
            this.LayoutV.btnDel.Click += btnDel_Click;
            this.LUC.ItemsList.SelectionChanged += ItemsList_SelectionChanged;
        }

        private void btnSelectSymbol_Click(object sender, RoutedEventArgs e)
        {
            MainVM.Card.SymbolId = this.UC.Symbol.SymbolId;
            this.LayoutV.NavigationService.GoBack();
            
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (this.checkValidity(this.UC.Symbol))
            {
                await MainVM.SymbolManager.Insert(this.UC.Symbol);
                InitLUC();
            }
        }

        private async void btnDel_Click(object sender, RoutedEventArgs e)
        {
            await MainVM.SymbolManager.Delete(this.UC.Symbol);
            InitLUC();
            InitUC();
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (this.checkValidity(this.UC.Symbol))
            {
                await MainVM.SymbolManager.Update(this.UC.Symbol);
                InitLUC();
            }
        }

        private Boolean checkValidity(Symbol symbol)
        {
            var regexName = new Regex(@"[a-zA-Z ]{3,30}");
            //var regexSalary = new Regex(@"\d+(,\d{1,2})?");

            if (regexName.IsMatch(symbol.Name)/* && regexSalary.IsMatch(job.Salary.ToString())*/)
            {
                return true;
            }
            else
            {
                System.Windows.MessageBox.Show("Please check fields");
                return false;
            }
        }
    }
}
