﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using pokerCSharp.views;
using pokerCSharp.database;
using pokerCSharp.entities;
using pokerCSharp.views.gameplay;

namespace pokerCSharp.viewmodel
{
    public class MainMenuVM
    {
        #region MainMenuView
        private MainMenuV mainMenuView;

        public MainMenuVM() { }

        public MainMenuVM(MainMenuV view)
        {
            this.mainMenuView = view;
            initMenuView();
            initPropertiesMainVM();
        }

        private void initPropertiesMainVM()
        {
            MainVM.SymbolManager = new MySQLManager<Symbol>();
            MainVM.CardManager = new MySQLManager<Card>();
            MainVM.UserManager = new MySQLManager<User>();
            MainVM.TableManager = new MySQLManager<Table>();
            MainVM.RankingManager = new MySQLManager<Ranking>();
            MainVM.User = new User();
            MainVM.Table = new Table();
            MainVM.Ranking = new Ranking();
            MainVM.Card = new Card();
            MainVM.Symbol = new Symbol();
            MainVM.Pot = new Pot();
        }

        private void initMenuView()
        {
            this.mainMenuView.UCMainMenuView.BtnAdministration.Click += clickAdministration;
            this.mainMenuView.UCMainMenuView.BtnNewGame.Click += clickNewGame;
        }

        private void clickAdministration(object sender, RoutedEventArgs e)
        {
            this.mainMenuView.NavigationService.Navigate(new MainAdministrationV());
        }

        private void clickNewGame(object sender, RoutedEventArgs e)
        {
            this.mainMenuView.NavigationService.Navigate(new GameV());
        }
        #endregion
    }
}
