﻿using Newtonsoft.Json;
using pokerCSharp.database;
using pokerCSharp.entities;
using pokerCSharp.entities.enums;
using pokerCSharp.views.gameplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace pokerCSharp.viewmodel.gameplay
{
    class GameVM
    {
        private GameV gameV;
        private List<Card> deck;
        private List<User> user;
        private List<int[]> possibleCombination;
        private Table table;
        private Dealer dealer;
        private Int16 cursorPlayer;
        private Int16 cursorTurn;
        private Int16 nbPlayerPlayed;
        private List<Card[]> allUsersCards;
        private List<Card> board;

        private Dispatcher UIDispatcher = Dispatcher.CurrentDispatcher;

        private const int NBMAXPLAYER = 2;
        private const int NBCARDPERPLAYER = 2;
        private const int NBMAXCARDBOARD = 5;
        private const int BB = 2;

        public GameVM() { }

        public GameVM(GameV view)
        {
            this.gameV = view;
            initGame();
        }

        private void initProperties()
        {
            this.deck = new List<Card>();
            this.user = new List<User>();
            this.possibleCombination = new List<int[]>();
            this.table = new Table();
            this.dealer = new Dealer();
            this.cursorPlayer = 1;
            this.cursorTurn = 0;
            this.nbPlayerPlayed = 0;
            this.allUsersCards = new List<Card[]>();
            this.board = new List<Card>();
        }

        private void initDealer()
        {
            dealer.Sprite = "dealer.png";

            String uriImageDealer = "C:\\Users\\kevin\\Documents\\Visual Studio 2017\\Projects\\pokerCSharp\\pokerCSharp\\img\\dealer\\" + dealer.Sprite;

            UIDispatcher.Invoke(new Action(() =>
            {
                this.gameV.gameUC.dealer.Fill = new ImageBrush(new BitmapImage(new Uri(uriImageDealer)));
            }), DispatcherPriority.ContextIdle);
        }

        private Boolean initHand()
        {
            (new Thread(async () =>
            {
                updateSolde();

                //0 Because it's not already init at first turn of first hand
                if (cursorTurn != 0)
                {
                    Thread.Sleep(4000);
                }

                cursorTurn = 1;

                moveDealer();
                initDealer();

                await initDeck();

                this.generateUserCard();
                this.generateBoardCard();

                initHiddenCard();

                if (cursorTurn == 1)
                {
                    if (cursorPlayer != 1)
                    {
                        user[1].Solde -= BB / 2;
                        user[0].Solde -= BB;
                        iaProcess();
                    }
                    else
                    {
                        user[0].Solde -= BB / 2;
                        user[1].Solde -= BB;
                        updateSolde();
                    }
                }

                MainVM.Pot.Amount += BB + BB / 2;
                this.gameV.potUC.Pot = MainVM.Pot;
            })).Start();

            Boolean initOK = true;
            return initOK;
        }

        //dealer top = left: 1080 top: 228
        //dealer bottom = left: 500 top: 595
        private void moveDealer()
        {
            if (dealer.Player == NBMAXPLAYER)
            {
                dealer.Player = 1;
                cursorPlayer = dealer.Player;
                UIDispatcher.Invoke(new Action(() =>
                {
                    Canvas.SetTop(this.gameV.gameUC.dealer, 595);
                    Canvas.SetLeft(this.gameV.gameUC.dealer, 500);
                }), DispatcherPriority.ContextIdle);
            }
            else
            {
                dealer.Player++;
                cursorPlayer = dealer.Player;
                UIDispatcher.Invoke(new Action(() =>
                {
                    Canvas.SetTop(this.gameV.gameUC.dealer, 228);
                    Canvas.SetLeft(this.gameV.gameUC.dealer, 1080);
                }), DispatcherPriority.ContextIdle);
            }
        }

        private IEnumerable<int[]> Combinations(int m, int n)
        {
            int[] result = new int[m];
            Stack<int> stack = new Stack<int>();
            stack.Push(0);

            while (stack.Count > 0)
            {
                int index = stack.Count - 1;
                int value = stack.Pop();

                while (value < n)
                {
                    result[index++] = ++value;
                    stack.Push(value);

                    if (index == m)
                    {
                        yield return result;
                        break;
                    }
                }
            }
        }

        private async void initGame()
        {
            initProperties();

            //Need refactor
            user.Add(await MainVM.UserManager.Get(1));

            for (int i = 1; i < NBMAXPLAYER; i++)
            {
                user.Add(new User { Name = "Ennemy" + i, Solde = 1500 });
            }

            initTable();

            initCombination();
            initActions();

            firstPlayer();

            initHand();
        }

        public async void showdown()
        {
            Dictionary<User, List<PokerHand>> allPokerHand = new Dictionary<User, List<PokerHand>>();

            for (int i = 0; i < NBMAXPLAYER; i++)
            {
                Card[] allCards = new Card[NBCARDPERPLAYER + NBMAXCARDBOARD];
                for (int j = 0; j < NBCARDPERPLAYER; j++)
                {

                    allCards[j] = allUsersCards[i][j];
                }

                for (int k = NBCARDPERPLAYER; k < (NBMAXCARDBOARD + NBCARDPERPLAYER); k++)
                {
                    allCards[k] = board[k - 2];
                }

                List<PokerHand> userCardCombination = new List<PokerHand>();

                foreach (int[] combination in possibleCombination)
                {
                    Card[] cards = new Card[combination.Length];
                    for (int l = 0; l < combination.Length; l++)
                    {
                        cards[l] = allCards[(combination[l] - 1)];
                    }

                    PokerHand pokerHand = new PokerHand();
                    pokerHand.Cards = cards;
                    pokerHand.Sort();
                    userCardCombination.Add(pokerHand);
                }

                allPokerHand.Add(user[i], userCardCombination);
            }

            Comparator handComparator = new Comparator();

            List<User> winners = (List<User>)handComparator.Evaluate(allPokerHand);

            int reward = MainVM.Pot.Amount / winners.Count;

            foreach (User user in winners)
            {
                user.Solde += reward;
            }

            MainVM.Pot.Amount = 0;

            this.gameV.potUC.Pot = MainVM.Pot;

            showEnnemyCard();

            await MainVM.UserManager.Update(user[0]);

            initHand();
        }

        private void updateSolde()
        {
            for (int i = 0; i < user.Count; i++)
            {
                UIDispatcher.Invoke(new Action(() => { ((TextBlock)this.gameV.gameUC.FindName("soldeUser" + (i + 1))).Text = user[i].Solde.ToString(); }), DispatcherPriority.ContextIdle);
            }
        }

        //Flop, turn ect ...
        private void nextRound()
        {
            if (cursorTurn == 4)
            {
                MainVM.Pot.LastRaise = 0;
                cursorTurn++;
                showdown();
            }
            else
            {
                MainVM.Pot.LastRaise = 0;

                cursorTurn++;
                showBoardCard();

                nextPlayer();
            }
        }

        private void nextPlayer()
        {
            nbPlayerPlayed++;

            if (cursorPlayer == 1)
            {
                cursorPlayer++;
            }
            else
            {
                cursorPlayer--;
            }

            if (nbPlayerPlayed < user.Count)
            {
                if (cursorPlayer != 1)
                {
                    if (cursorTurn != 1)
                    {
                        nbPlayerPlayed--;

                    }

                    iaProcess();
                }
            }
            else
            {
                nbPlayerPlayed = 0;
                cursorPlayer = dealer.Player;
                nextRound();
            }


            updateSolde();
        }

        public void iaProcess()
        {
            if (cursorPlayer == dealer.Player)
            {
                if (cursorTurn == 1)
                {
                    if (MainVM.Pot.LastRaise != 0)
                    {
                        if (user[1].Solde >= MainVM.Pot.LastRaise - BB)
                        {
                            MainVM.Pot.LastRaise = MainVM.Pot.LastRaise;
                            MainVM.Pot.Amount += MainVM.Pot.LastRaise - BB;
                            user[1].Solde -= MainVM.Pot.LastRaise - BB;
                            nbPlayerPlayed++;
                        }
                        else
                        {
                            sumProfit(0);

                            user[1].Solde = 1500;

                            initHand();
                        }
                    }
                    else if (user[1].Solde >= BB)
                    {
                        MainVM.Pot.Amount += (BB - BB / 2);
                        user[1].Solde -= (BB - BB / 2);
                        MainVM.Pot.LastRaise = BB;
                    }
                    else
                    {
                        sumProfit(0);

                        user[1].Solde = 1500;

                        initHand();
                    }
                }
                else
                {
                    if (MainVM.Pot.LastRaise != 0)
                    {
                        if (user[1].Solde >= MainVM.Pot.LastRaise)
                        {
                            MainVM.Pot.LastRaise = MainVM.Pot.LastRaise;
                            MainVM.Pot.Amount += MainVM.Pot.LastRaise;
                            user[1].Solde -= MainVM.Pot.LastRaise;
                            nbPlayerPlayed++;
                        }
                        else
                        {
                            sumProfit(0);

                            user[1].Solde = 1500;

                            initHand();
                        }
                    }
                }
            }
            else
            {
                if (cursorTurn == 1)
                {
                    if (MainVM.Pot.LastRaise != 0)
                    {
                        if (user[1].Solde >= MainVM.Pot.LastRaise - BB)
                        {
                            MainVM.Pot.Amount += MainVM.Pot.LastRaise - BB;
                            user[1].Solde -= MainVM.Pot.LastRaise - BB;
                        }
                        else
                        {
                            sumProfit(0);

                            user[1].Solde = 1500;

                            initHand();
                        }
                    }
                }
                else
                {
                    if (MainVM.Pot.LastRaise != 0)
                    {
                        if (user[1].Solde >= MainVM.Pot.LastRaise - BB)
                        {
                            nbPlayerPlayed++;
                            MainVM.Pot.LastRaise = MainVM.Pot.LastRaise;
                            MainVM.Pot.Amount += MainVM.Pot.LastRaise - BB;
                            user[1].Solde -= MainVM.Pot.LastRaise - BB;
                        }
                        else
                        {
                            sumProfit(0);

                            user[1].Solde = 1500;

                            initHand();
                        }
                    }
                    else
                    {
                        if (user[1].Solde >= BB)
                        {
                            MainVM.Pot.LastRaise = BB;
                            MainVM.Pot.Amount += BB;
                            user[1].Solde -= BB;
                        }
                        else
                        {
                            sumProfit(0);

                            user[1].Solde = 1500;

                            initHand();
                        }
                    }
                }
            }

            this.gameV.potUC.Pot = MainVM.Pot;
            nextPlayer();
        }

        private Card[] generateCards()
        {
            Card[] cards = new Card[NBCARDPERPLAYER];

            for (int j = 0; j < NBCARDPERPLAYER; j++)
            {
                cards[j] = generateCard();
            }

            return cards;
        }

        public Card generateCard()
        {
            Card card = new Card();

            using (RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider())
            {
                byte[] rno = new byte[5];
                rg.GetBytes(rno);
                int randomvalue = Math.Abs(BitConverter.ToInt32(rno, 0) % (deck.Count - 1) + 1);
                card = deck[randomvalue];
                deck.RemoveAt(randomvalue);
            }

            return card;
        }

        private void generateUserCard()
        {
            allUsersCards = new List<Card[]>();

            int k = 1;

            for (int i = 0; i < NBMAXPLAYER; i++)
            {
                Card[] cards = new Card[2];
                cards = generateCards();

                allUsersCards.Add(cards);
            }

            for (k = 0; k < NBCARDPERPLAYER; k++)
            {
                String uriImageCard = "C:\\Users\\kevin\\Documents\\Visual Studio 2017\\Projects\\pokerCSharp\\pokerCSharp\\img\\cards\\" + allUsersCards[0][k].Sprite;
                UIDispatcher.Invoke(new Action(() =>
                {
                    ((Rectangle)this.gameV.gameUC.FindName("userCard" + (k + 1))).Fill = new ImageBrush(new BitmapImage(new Uri(uriImageCard)));
                }), DispatcherPriority.ContextIdle);
            }
        }

        private void showEnnemyCard()
        {
            for (int k = 0; k < NBCARDPERPLAYER; k++)
            {
                String uriImageCard = "C:\\Users\\kevin\\Documents\\Visual Studio 2017\\Projects\\pokerCSharp\\pokerCSharp\\img\\cards\\" + allUsersCards[1][k].Sprite;
                UIDispatcher.Invoke(new Action(() =>
                {
                    ((Rectangle)this.gameV.gameUC.FindName("userCard" + (k + 3))).Fill = new ImageBrush(new BitmapImage(new Uri(uriImageCard)));
                }), DispatcherPriority.ContextIdle);
            }
        }

        private void showBoardCard()
        {
            int k = 0;
            int max = 0;

            if (cursorTurn == 2)
            {
                k = 0;
                max = NBMAXCARDBOARD - 2;
            }
            else if (cursorTurn == 3)
            {
                k = NBMAXCARDBOARD - 2;
                max = NBMAXCARDBOARD - 1;
            }
            else
            {
                k = NBMAXCARDBOARD - 1;
                max = NBMAXCARDBOARD;
            }

            for (; k < max; k++)
            {
                String uriImageCard = "C:\\Users\\kevin\\Documents\\Visual Studio 2017\\Projects\\pokerCSharp\\pokerCSharp\\img\\cards\\" + board[k].Sprite;

                UIDispatcher.Invoke(new Action(() =>
                {
                    ((Rectangle)this.gameV.gameUC.FindName("boardCard" + (k + 1))).Fill = new ImageBrush(new BitmapImage(new Uri(uriImageCard)));
                }), DispatcherPriority.ContextIdle);
            }
        }

        private void generateBoardCard()
        {
            for (int i = 0; i < NBMAXCARDBOARD; i++)
            {
                Card card = new Card();
                card = generateCard();
                board.Add(card);
            }
        }

        private void initActions()
        {
            this.gameV.gameUC.raiseBtn.Click += clickRaise;
            this.gameV.gameUC.checkcallBtn.Click += clickCallCheck;
            this.gameV.gameUC.foldBtn.Click += clickFold;
        }

        private void clickRaise(object sender, RoutedEventArgs e)
        {
            if (cursorTurn < 5)
            {
                Int32 userAmount = 0;

                if (cursorPlayer == dealer.Player)
                {
                    if (cursorTurn == 1)
                    {
                        if (Int32.TryParse(this.gameV.gameUC.userAmount.Text, out userAmount))
                        {
                            if (user[0].Solde >= userAmount - BB / 2)
                            {
                                if (userAmount >= BB * 2)
                                {
                                    MainVM.Pot.LastRaise = userAmount;
                                    MainVM.Pot.Amount += userAmount - BB / 2;
                                    user[0].Solde -= userAmount - BB / 2;
                                }
                                else
                                {
                                    return;
                                }
                            }
                            else
                            {
                                sumProfit(0);

                                user[1].Solde = 1500;

                                initHand();
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (Int32.TryParse(this.gameV.gameUC.userAmount.Text, out userAmount))
                        {
                            if (user[0].Solde >= userAmount)
                            {
                                if (userAmount >= BB * 2)
                                {
                                    MainVM.Pot.LastRaise = userAmount;
                                    MainVM.Pot.Amount += userAmount;
                                    user[0].Solde -= userAmount;
                                    nbPlayerPlayed--;
                                }
                                else
                                {
                                    return;
                                }
                            }
                            else
                            {
                                sumProfit(0);

                                user[1].Solde = 1500;

                                initHand();
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    if (cursorTurn == 1)
                    {
                        if (Int32.TryParse(this.gameV.gameUC.userAmount.Text, out userAmount))
                        {
                            if (user[0].Solde >= userAmount - BB)
                            {
                                if (userAmount >= BB * 2)
                                {
                                    MainVM.Pot.LastRaise = userAmount;
                                    MainVM.Pot.Amount += userAmount - BB;
                                    user[0].Solde -= userAmount - BB;
                                    nbPlayerPlayed--;
                                }
                                else
                                {
                                    return;
                                }
                            }
                            else
                            {
                                sumProfit(0);

                                user[1].Solde = 1500;

                                initHand();
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (Int32.TryParse(this.gameV.gameUC.userAmount.Text, out userAmount))
                        {
                            if (user[0].Solde >= userAmount)
                            {
                                if (userAmount >= BB * 2)
                                {
                                    MainVM.Pot.LastRaise = userAmount;
                                    MainVM.Pot.Amount += userAmount;
                                    user[0].Solde -= userAmount;
                                    nbPlayerPlayed--;
                                }
                                else
                                {
                                    return;
                                }
                            }
                            else
                            {
                                sumProfit(0);

                                user[1].Solde = 1500;

                                initHand();
                            }
                        }
                        else
                        {
                            return;
                        }
                    } 
                }

                this.gameV.potUC.Pot = MainVM.Pot;
                nextPlayer();
            }
        }


        private void clickCallCheck(object sender, RoutedEventArgs e)
        {
            if (cursorTurn < 5)
            {
                if (cursorPlayer == dealer.Player)
                {
                    if (cursorTurn == 1)
                    {
                        if (user[0].Solde >= BB / 2)
                        {
                            MainVM.Pot.LastRaise = BB;
                            MainVM.Pot.Amount += BB / 2;
                            user[0].Solde -= BB / 2;
                        }
                        else
                        {
                            sumProfit(0);

                            user[1].Solde = 1500;

                            initHand();
                        }
                    }
                    else
                    {
                        if (MainVM.Pot.LastRaise == BB)
                        {
                            MainVM.Pot.LastRaise = BB;
                            MainVM.Pot.Amount += BB;
                            user[0].Solde -= BB;
                        }
                    }
                }
                else
                {
                    if (cursorTurn == 1)
                    {

                    }
                    else
                    {

                    }
                }

                this.gameV.potUC.Pot = MainVM.Pot;
                nextPlayer();
            }
        }

        private void clickFold(object sender, RoutedEventArgs e)
        {
            if (cursorTurn < 5)
            {
                if ((cursorTurn != 1) || cursorPlayer == dealer.Player)
                {
                    allUsersCards.RemoveAt(0);

                    sumProfit(1);

                    initHand();
                }
            }
        }

        private async void sumProfit(Int32 indexUser)
        {
            user[indexUser].Solde += MainVM.Pot.Amount;

            MainVM.Pot.Amount = 0;

            this.gameV.potUC.Pot = MainVM.Pot;

            if (indexUser == 0)
            {
                await MainVM.UserManager.Update(user[0]);
            }
        }

        private void initCombination()
        {
            List<int[]> combinationPossible = new List<int[]>();

            foreach (int[] item in Combinations(5, 7))
            {
                int[] combination = new int[item.Length];

                for (int i = 0; i < combination.Length; i++)
                {
                    combination[i] = item[i];
                }

                possibleCombination.Add(combination);
            }
        }

        private async Task<Boolean> initDeck()
        {
            deck = (List<Card>)await MainVM.CardManager.Get();

            Boolean initOK = true;
            return initOK;
        }

        private async void initTable()
        {
            table = await MainVM.TableManager.Get(1);

            this.gameV.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#1f2122"));

            String uriImageTable = "C:\\Users\\kevin\\Documents\\Visual Studio 2017\\Projects\\pokerCSharp\\pokerCSharp\\img\\tables\\" + table.Sprite;

            this.gameV.gameCanvas.Background = new ImageBrush(new BitmapImage(new Uri(uriImageTable)));

            initPot();
        }

        private void firstPlayer()
        {
            using (RNGCryptoServiceProvider rg = new RNGCryptoServiceProvider())
            {
                byte[] rno = new byte[5];
                rg.GetBytes(rno);
                int randomValue = BitConverter.ToInt32(rno, 0);
                if (randomValue > 0)
                {
                    dealer.Player = 1;
                }
                else if (randomValue == 0)
                {
                    firstPlayer();
                }
                else
                {
                    dealer.Player = 2;
                }

                dealer.Player = 1;
            }
        }

        private void initPot()
        {
            MainVM.Pot = new Pot { Amount = 0, LastRaise = 0 };
            this.gameV.potUC.Pot = MainVM.Pot;
        }

        private void initHiddenCard()
        {
            String uriImage = "C:\\Users\\kevin\\Documents\\Visual Studio 2017\\Projects\\pokerCSharp\\pokerCSharp\\img\\cards\\cardBack_red3.png";

            for (int k = 3; k <= (NBCARDPERPLAYER * NBMAXPLAYER); k++)
            {
                UIDispatcher.Invoke(new Action(() =>
                {
                    ((Rectangle)this.gameV.gameUC.FindName("userCard" + k)).Fill = new ImageBrush(new BitmapImage(new Uri(uriImage)));
                }), DispatcherPriority.ContextIdle);
            }

            for (int k = 1; k <= 5; k++)
            {
                UIDispatcher.Invoke(new Action(() =>
                {
                    ((Rectangle)this.gameV.gameUC.FindName("boardCard" + k)).Fill = new ImageBrush(new BitmapImage(new Uri(uriImage)));
                }), DispatcherPriority.ContextIdle);
            }
        }
    }
}