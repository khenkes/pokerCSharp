﻿using pokerCSharp.database;
using pokerCSharp.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.viewmodel
{
    static class MainVM
    {
        internal static MySQLManager<Symbol> SymbolManager { get; set; }
        internal static MySQLManager<Card> CardManager { get; set; }
        internal static MySQLManager<User> UserManager { get; set; }
        internal static MySQLManager<Table> TableManager { get; set; }
        internal static MySQLManager<Ranking> RankingManager { get; set; }
        internal static Card Card { get; set; }
        internal static Symbol Symbol { get; set; }
        internal static User User { get; set; }
        internal static Table Table { get; set; }
        internal static Ranking Ranking { get; set; }
        internal static Pot Pot { get; set; }
    }
}
