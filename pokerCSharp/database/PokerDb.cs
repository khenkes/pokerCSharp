namespace pokerCSharp.database
{
    using pokerCSharp.entities;
    using pokerCSharp.entities.json;
    using System;
    using System.Data.Entity;
    using System.Linq;

    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class PokerDb : DbContext
    {
        public DbSet<Card> Cards { get; set; }
        public DbSet<Symbol> Symbols { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Ranking> Rankings { get; set; }

        public PokerDb()
            : base(JsonManager.Instance.ReadFile<ConnectionString>(@"..\..\..\jsonconfig\", @"MysqlConfig.json").ToString())
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}