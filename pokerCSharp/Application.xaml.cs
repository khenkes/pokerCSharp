﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using pokerCSharp.database;
using pokerCSharp.entities;
using pokerCSharp.views;
using pokerCSharp.views.administration;
using System.Data.Entity;
using Newtonsoft.Json;
using pokerCSharp.entities.enums;

namespace pokerCSharp
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class Application : NavigationWindow
    {
        public Application()
        {
            InitializeComponent();
            this.Content = new MainMenuV();
            testDb();
        }

        public async void testDb()
        {
            using (var db = new PokerDb())
            {
                if (db.Database.CreateIfNotExists())
                {
                    MySQLManager<Symbol> symbolManager = new MySQLManager<Symbol>();
                    MySQLManager<Ranking> rankingManager = new MySQLManager<Ranking>();
                    MySQLManager<Card> cardManager = new MySQLManager<Card>();
                    MySQLManager<User> userManager = new MySQLManager<User>();
                    MySQLManager<Table> tableManager = new MySQLManager<Table>();

                    List<Symbol> listSymbol = new List<Symbol>();
                    listSymbol.InsertRange(listSymbol.Count,
                        new Symbol[] {
                            new Symbol { Name = "Heart", Value = SuitType.Hearts },
                            new Symbol { Name = "Spade", Value = SuitType.Spades },
                            new Symbol { Name = "Club", Value = SuitType.Clubs },
                            new Symbol { Name = "Diamond", Value = SuitType.Diamonds }
                        }
                    );

                    List<Ranking> listRanking = new List<Ranking>();

                    listRanking.InsertRange(listRanking.Count, new Ranking[] {
                    new Ranking { Name = "Two", Value = RankType.Two },
                    new Ranking { Name = "Three", Value = RankType.Three },
                    new Ranking { Name = "Four", Value = RankType.Four },
                    new Ranking { Name = "Five", Value = RankType.Five },
                    new Ranking { Name = "Six", Value = RankType.Six },
                    new Ranking { Name = "Seven", Value = RankType.Seven },
                    new Ranking { Name = "Eight", Value = RankType.Eight },
                    new Ranking { Name = "Nine", Value = RankType.Nine },
                    new Ranking { Name = "Ten", Value = RankType.Ten },
                    new Ranking { Name = "Jack", Value = RankType.Jack },
                    new Ranking { Name = "Queen", Value = RankType.Queen },
                    new Ranking { Name = "King", Value = RankType.King },
                    new Ranking { Name = "Ace", Value = RankType.Ace }
                    });
                    

                    listSymbol = (List<Symbol>)await symbolManager.Insert(listSymbol);
                    listRanking = (List<Ranking>)await rankingManager.Insert(listRanking);

                    List<Card> listCard = new List<Card>();

                    listRanking.ForEach( ranking =>
                    {
                        listSymbol.ForEach(symbol =>
                        {
                            listCard.Add(new Card { Sprite = "card" + symbol.Name + "s" + ((Int32) ranking.Value - 1) + ".png", SymbolId = symbol.SymbolId, Symbol = symbol, RankingId = ranking.RankingId, Ranking = ranking });
                        });
                    });

                    User user = new User { Name = "test", Solde = 1500 };
                    
                    Table table = new Table { Sprite = "table1.png" };

                    listCard = (List<Card>) await cardManager.Insert(listCard);
                    user = (User) await userManager.Insert(user);
                    table = (Table) await tableManager.Insert(table);
                }
            }
        }
    }
}
