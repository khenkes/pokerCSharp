﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pokerCSharp.entities;
using System.ComponentModel.DataAnnotations;

namespace pokerCSharp.entities
{
    public class Ranking : BaseEntity
    {
        [Key]
        public Int32 RankingId { get; set; }
        public String Name { get; set; }
        public RankType Value { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
    }
}