﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace pokerCSharp.entities.json
{
    public class JsonManager
    {
        private static volatile pokerCSharp.entities.json.JsonManager instance;
        private static object syncRoot = new Object();

        private JsonManager() { }

        public static pokerCSharp.entities.json.JsonManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new pokerCSharp.entities.json.JsonManager();
                    }
                }

                return instance;
            }
        }


        public T ReadFile<T>(String path, String file)
        {

            T toReturn = default(T);
            using (StreamReader fileItem = File.OpenText(path + file))
            using (JsonTextReader reader = new JsonTextReader(fileItem))
            {
                JObject jObject = (JObject)JToken.ReadFrom(reader);
                toReturn = jObject.ToObject<T>();
            }

            return toReturn;
        }
    }
}
