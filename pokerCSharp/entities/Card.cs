﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pokerCSharp.entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using pokerCSharp.entities.enums;

namespace pokerCSharp.entities
{
    public class Card : BaseEntity
    {
        [Key]
        public Int32 CardId { get; set; }
        public String Sprite { get; set; }
        public Int32 RankingId { get; set; }
        [ForeignKey("RankingId")]
        public virtual Ranking Ranking { get; set; }
        public Int32 SymbolId { get; set; }
        [ForeignKey("SymbolId")]
        public virtual Symbol Symbol { get; set; }

        public Card()
        {

        }
    }
}
