﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pokerCSharp.entities
{
    public class User : BaseEntity
    {
        [Key]
        public Int32 UserId { get; set; }
        public String Name { get; set; }
        public Double Solde { get; set; }
    }
}
