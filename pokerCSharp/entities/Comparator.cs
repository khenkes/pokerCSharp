﻿using pokerCSharp.entities.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.entities
{
    class Comparator
    {
        public IList<User> Evaluate(IDictionary<User, List<PokerHand>> hands)
        {
            var len = Enum.GetValues(typeof(HandType)).Length;
            var winners = new List<User>();
            HandType winningType = HandType.HighCard;

            Int32 bestHand = 0;

            var allHands = hands.Values.ToList();
            var allUsers = hands.Keys.ToList();

            for (int j = 0; j < allHands[0].Count; j++)
            {
                for (int i = 0; i < hands.Keys.Count; i++)
                {
                    var hand = allHands[i][j];

                    for (var handType = HandType.RoyalFlush;
                    (int)handType < len; handType = handType + 1)
                    {
                        if (hand.IsValid(handType))
                        {
                            int compareHands = 0, compareCards = 0;
                            if (winners.Count == 0
                             || (compareHands = winningType.CompareTo(handType)) > 0
                             || compareHands == 0
                              && (compareCards = hand.CompareTo(hands[winners[0]][bestHand])) >= 0)
                            {
                                bestHand = j;
                                if (compareHands > 0 || compareCards > 0) winners.Clear();
                                winners.Add(allUsers[i]);
                                winningType = handType;
                            }
                            break;
                        }
                    }
                }
            }

            Console.WriteLine(winners[0].Name);
            Console.WriteLine(winningType);
            Console.WriteLine(hands[winners[0]][bestHand].ToString());

            return winners;
        }
    }
}
