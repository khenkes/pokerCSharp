﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.entities
{
    public class Pot : BaseEntity
    {
        public Int32 Amount { get; set; }
        public Int32 LastRaise { get; set; }
    }
}
