﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pokerCSharp.entities;
using System.ComponentModel.DataAnnotations;

namespace pokerCSharp.entities
{
    public class Table : BaseEntity
    {
        [Key]
        public Int32 TableId { get; set; }
        public String Sprite { get; set;  }
    }
}
