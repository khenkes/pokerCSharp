﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.entities.enums
{
    public enum HandType : int
    {
        RoyalFlush, StraightFlush, FourOfAKind,
        FullHouse, Flush, Straight, ThreeOfAKind, TwoPairs, OnePair, HighCard
    }
}
