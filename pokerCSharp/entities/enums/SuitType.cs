﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.entities.enums
{
    public enum SuitType : int
    {
        Spades, Hearts, Diamonds, Clubs
    }
}
