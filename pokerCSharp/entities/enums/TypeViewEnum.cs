﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.entities.enums
{
        public sealed class TypeViewEnum
        {

            private readonly String name;
            private readonly int value;

            public static readonly TypeViewEnum CARD = new TypeViewEnum(1, "Card");
            public static readonly TypeViewEnum PATTERNCARD = new TypeViewEnum(2, "PatternCard");
            public static readonly TypeViewEnum RANKING = new TypeViewEnum(3, "Ranking");
            public static readonly TypeViewEnum SYMBOL = new TypeViewEnum(4, "Symbol");
            public static readonly TypeViewEnum TABLE = new TypeViewEnum(5, "Table");
            public static readonly TypeViewEnum USER = new TypeViewEnum(6, "User");

        private TypeViewEnum(int value, String name)
            {
                this.name = name;
                this.value = value;
            }

            public override String ToString()
            {
                return name;
            }

        }
}
