﻿using pokerCSharp.entities.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokerCSharp.entities
{
    class PokerHand : IComparable<PokerHand>
    {
        public Card[] Cards { get; set; }

        public PokerHand()
        {

        }

        public PokerHand(Card c1, Card c2, Card c3, Card c4, Card c5)
        {
            Cards = new Card[] { c1, c2, c3, c4, c5 };
            Sort();
        }

        public void Sort()
        {
            //this.Cards = Cards.OrderBy(c => c.Ranking.Value)
            // .OrderBy(c => Cards.Where(c1 => c1.Ranking.Value == c.Ranking.Value).Count()).ToArray();

            //if (Cards[4].Ranking.Value == RankType.Ace && Cards[0].Ranking.Value == RankType.Two
            // && (int)Cards[3].Ranking.Value - (int)Cards[0].Ranking.Value == 3)
            //    Cards = new Card[] { Cards[4], Cards[0], Cards[1], Cards[2], Cards[3] };

            Cards = Cards.OrderBy(c => c.Ranking.Value).ToArray();

            if (Cards[4].Ranking.Value == RankType.Ace && Cards[0].Ranking.Value == RankType.Two
             && (int)Cards[3].Ranking.Value - (int)Cards[0].Ranking.Value == 3)
                Cards = new Card[] { Cards[4], Cards[0], Cards[1], Cards[2], Cards[3] };
        }

        public int CompareTo(PokerHand other)
        {
            for (var i = 4; i >= 0; i--)
            {
                RankType rank1 = Cards[i].Ranking.Value, rank2 = other.Cards[i].Ranking.Value;
                if (rank1 > rank2) return 1;
                if (rank1 < rank2) return -1;
            }
            return 0;
        }

        public bool IsValid(HandType handType)
        {
            switch (handType)
            {
                case HandType.RoyalFlush:
                    return IsValid(HandType.StraightFlush) && Cards[4].Ranking.Value == RankType.Ace;
                case HandType.StraightFlush:
                    return IsValid(HandType.Flush) && IsValid(HandType.Straight);
                case HandType.FourOfAKind:
                    return GetGroupByRankCount(4) == 1;
                case HandType.FullHouse:
                    return IsValid(HandType.ThreeOfAKind) && IsValid(HandType.OnePair);
                case HandType.Flush:
                    return GetGroupBySuitCount(5) == 1;
                case HandType.Straight:
                    return (int)Cards[4].Ranking.Value - (int)Cards[0].Ranking.Value == 4
                     || Cards[0].Ranking.Value == RankType.Ace;
                case HandType.ThreeOfAKind:
                    return GetGroupByRankCount(3) == 1;
                case HandType.TwoPairs:
                    return GetGroupByRankCount(2) == 2;
                case HandType.OnePair:
                    return GetGroupByRankCount(2) == 1;
                case HandType.HighCard:
                    return GetGroupByRankCount(1) == 5;
            }
            return false;
        }

        private int GetGroupByRankCount(int n)
        { return Cards.GroupBy(c => c.Ranking.Value).Count(g => g.Count() == n); }

        private int GetGroupBySuitCount(int n)
        { return Cards.GroupBy(c => c.Symbol.Value).Count(g => g.Count() == n); }

        override
        public String ToString()
        {
            String output = "";
            new List<Card>(Cards).ForEach( card =>
            {
                output += card.Ranking.Name + " ";
                output += card.Symbol.Name + ", ";
            });

            return output;
        }
    }

}

