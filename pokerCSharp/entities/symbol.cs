﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using pokerCSharp.entities.enums;

namespace pokerCSharp.entities
{
    public class Symbol : BaseEntity
    {
        [Key]
        public Int32 SymbolId { get; set; }
        public String Name { get; set; }
        public SuitType Value { get; set; }
        public virtual ICollection<Card> Cards { get; set; }
    }
}
