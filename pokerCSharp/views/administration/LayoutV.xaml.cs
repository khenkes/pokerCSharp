﻿using pokerCSharp.entities;
using pokerCSharp.entities.enums;
using pokerCSharp.viewmodel.administration;
using pokerCSharp.views.usercontrols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.administration
{
    /// <summary>
    /// Interaction logic for LayoutV.xaml
    /// </summary>
    public partial class LayoutV : Page
    {
        public LayoutV(String type)
        {
            InitializeComponent();
            LayoutVM layoutVM = new LayoutVM(this, type);
        }

        public LayoutV(String type, bool extend)
        {
            InitializeComponent();
            LayoutVM layoutVM = new LayoutVM(this, type, true);
        }
    }
}
