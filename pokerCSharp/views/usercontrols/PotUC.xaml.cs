﻿using pokerCSharp.entities;
using pokerCSharp.viewmodel;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Interaction logic for PotUC.xaml
    /// </summary>
    public partial class PotUC : BaseUC
    {
        private Pot pot = MainVM.Pot;

        public Pot Pot
        {
            get { return pot; }
            set
            {
                pot = value;
                base.OnPropertyChanged("Pot");
            }
        }

        public PotUC()
        {
            InitializeComponent();
            base.DataContext = this;
        }
    }
}
