﻿using pokerCSharp.entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Interaction logic for SymbolLUC.xaml
    /// </summary>
    public partial class RankingLUC : BaseLUC
    {
        public ListView ItemsList { get; set; }
        public ObservableCollection<Ranking> Obs { get; set; }

        public RankingLUC()
        {
            this.InitializeComponent();
            Obs = new ObservableCollection<Ranking>();
            this.itemList.ItemsSource = Obs;
            this.ItemsList = this.itemList;
            this.ItemsList.SelectionMode = SelectionMode.Single;
        }

        /// <summary>
        /// Current list for User items.
        /// </summary>
        public void LoadItem(List<Ranking> items)
        {
            Obs.Clear();
            foreach (var item in items)
            {
                Obs.Add(item);
            }
        }
    }
}
