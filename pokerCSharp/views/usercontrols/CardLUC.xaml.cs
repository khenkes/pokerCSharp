﻿using pokerCSharp.entities;
using pokerCSharp.views.usercontrols;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Interaction logic for CardLUC.xaml
    /// </summary>
    public partial class CardLUC : BaseLUC
    {
        public ListView ItemsList { get; set; }
        public ObservableCollection<Card> Obs { get; set; }

        public CardLUC()
        {
            this.InitializeComponent();
            Obs = new ObservableCollection<Card>();
            this.itemList.ItemsSource = Obs;
            this.ItemsList = this.itemList;
            this.ItemsList.SelectionMode = SelectionMode.Single;
        }

        /// <summary>
        /// Current list for User items.
        /// </summary>
        public void LoadItem(List<Card> items)
        {
            Obs.Clear();
            foreach (var item in items)
            {
                Obs.Add(item);
            }
        }

        /*private void RemoveJobContextMenu_OnClick(object sender, RoutedEventArgs e)
        {
            Obs.Remove(ItemsList.SelectedItem as Job);
        }

        private async void DuplicateJobContextMenu_OnClick(object sender, RoutedEventArgs e)
        {
            if (ItemsList.SelectedIndex > -1)
            {
                var duplicateJob = new Job();
                duplicateJob = (Job)ItemsList.SelectedItem;
                duplicateJob.Id = 0;
                await jobManager.Insert(duplicateJob);

                this.LoadItem((await jobManager.Get()).ToList());

            }
        }*/
    }
}
