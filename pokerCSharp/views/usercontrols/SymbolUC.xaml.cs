﻿using pokerCSharp.entities;
using pokerCSharp.viewmodel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Interaction logic for SymbolUC.xaml
    /// </summary>
    public partial class SymbolUC : BaseUC
    {
        private Symbol symbol = MainVM.Symbol;

        public Symbol Symbol
        {
            get { return symbol; }
            set
            {
                symbol = value;
                base.OnPropertyChanged("Symbol");
            }
        }

        public SymbolUC()
        {
            InitializeComponent();
            base.DataContext = this;
            //if (Symbol != null)
            //{
            //    this.ucSchedule.Schedule = Job.Schedule;
            //}
        }
    }
}
