﻿using pokerCSharp.entities;
using pokerCSharp.viewmodel;
using pokerCSharp.views.usercontrols;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Interaction logic for CardUC.xaml
    /// </summary>
    public partial class CardUC : BaseUC
    {
        private Card card = MainVM.Card;

        public Card Card
        {
            get { return card; }
            set
            {
                card = value;
                base.OnPropertyChanged("Card");
            }
        }

        public CardUC()
        {
            InitializeComponent();
            base.DataContext = this;
        }
    }
}
