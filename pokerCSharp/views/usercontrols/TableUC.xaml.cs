﻿using pokerCSharp.entities;
using pokerCSharp.viewmodel;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Logique d'interaction pour AddressUserControl.xaml
    /// </summary>
    public partial class TableUC : BaseUC
    {
        private Table table = MainVM.Table;

        public Table Table
        {
            get { return table; }
            set
            {
                table = value;
                base.OnPropertyChanged("Table");
            }
        }

        public TableUC()
        {
            InitializeComponent();
            base.DataContext = this;
            //if (Symbol != null)
            //{
            //    this.ucSchedule.Schedule = Job.Schedule;
            //}
        }
    }
}
