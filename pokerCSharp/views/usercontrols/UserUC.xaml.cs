﻿using pokerCSharp.entities;
using pokerCSharp.viewmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Interaction logic for UserUC.xaml
    /// </summary>
    public partial class UserUC : BaseUC
    {
        private User user = MainVM.User;

        public User User
        {
            get { return user; }
            set
            {
                user = value;
                base.OnPropertyChanged("User");
            }
        }

        public UserUC()
        {
            InitializeComponent();
            base.DataContext = this;
        }
    }
}
