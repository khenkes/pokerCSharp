﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace pokerCSharp.views.usercontrols
{
public class BaseUC : UserControl, INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged(String name)
    {
        PropertyChangedEventHandler handler = PropertyChanged;

        if (handler != null)

        {
            handler(this, new PropertyChangedEventArgs(name));
        }
    }
}
}
