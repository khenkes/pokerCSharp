﻿using pokerCSharp.entities;
using pokerCSharp.views.usercontrols;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.usercontrols
{
    /// <summary>
    /// Logique d'interaction pour ListAddressUserControl.xaml
    /// </summary>
    public partial class TableLUC : BaseLUC
    {
        public ListView ItemsList { get; set; }
        public ObservableCollection<Table> Obs { get; set; }

        public TableLUC()
        {
            this.InitializeComponent();
            Obs = new ObservableCollection<Table>();
            this.itemList.ItemsSource = Obs;
            this.ItemsList = this.itemList;
            this.ItemsList.SelectionMode = SelectionMode.Single;
        }

        /// <summary>
        /// Current list for User items.
        /// </summary>
        public void LoadItem(List<Table> items)
        {
            Obs.Clear();
            foreach (var item in items)
            {
                Obs.Add(item);
            }
        }
    }
}
