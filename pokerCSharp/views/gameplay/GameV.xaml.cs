﻿using pokerCSharp.entities;
using pokerCSharp.viewmodel.gameplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokerCSharp.views.gameplay
{
    /// <summary>
    /// Interaction logic for GameV.xaml
    /// </summary>
    public partial class GameV : Page
    {
        public GameV()
        {
            InitializeComponent();
            GameVM gameVM = new GameVM(this);
            this.DataContext = gameVM;
        }
    }
}
